import IngredientTable from './'
import {
  ingredientData,
  actionsData,
} from '../IngredientRow/IngredientRow.stories'

const paddedTable = () => {
  return {
    template: `<div style="padding: 3rem;"><story /></div>`,
  }
}

export default {
  title: 'IngredientTable',
  decorators: [paddedTable],
  excludeStories: /.*Data$/,
}

export const defaultIngredientsData = [
  { ...ingredientData },
  { ...ingredientData, name: 'Yeast', weight: 1, ratio: 1 },
  { ...ingredientData, name: 'Water', weight: 50, ratio: 50 },
]

const ingredientTableTemplate = `<IngredientTable :ingredients="ingredients" :header="header" @removeIngredient="onRemoveIngredient" />`

export const With3Ingredients = () => ({
  components: { IngredientTable },
  template: ingredientTableTemplate,
  props: {
    ingredients: {
      default: () => defaultIngredientsData,
    },
  },
  methods: actionsData,
})

export const With3IngredientsHeader = () => ({
  components: { IngredientTable },
  template: ingredientTableTemplate,
  props: {
    header: {
      default: () => ['Name', 'Weight (g)', 'Percentage', ''],
    },
    ingredients: {
      default: () => defaultIngredientsData,
    },
  },
  methods: actionsData,
})
