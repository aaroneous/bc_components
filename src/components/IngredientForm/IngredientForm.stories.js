import { action } from '@storybook/addon-actions'
import IngredientForm from './'

const paddedForm = () => {
  return {
    template: `<div style="padding: 3rem;"><story /></div>`,
  }
}

export default {
  title: 'IngredientForm',
  decorators: [paddedForm],
  excludeStories: /.*Data$/,
}

const actionWithPreventDefault = (name) => (e) => {
  e.preventDefault()
  action(name)(e)
}

export const actionsData = {
  onAddIngredient: actionWithPreventDefault('onAddIngredient'),
  onEnteredName: action('onEnteredName'),
}

export const optionsData = {
  ingredientOptions: ['Flour', 'Milk', 'Eggs'],
  dataOptions: ['Percentage', 'Weight (g)'],
}

const ingredientFormTemplate = `<ingredient-form :ingredientOptions="ingredientOptions" :dataOptions="dataOptions" @addedIngredient="onAddIngredient" @enteredName="onEnteredName" />`

export const Default = () => ({
  components: { IngredientForm },
  template: ingredientFormTemplate,
  props: {
    ingredientOptions: {
      default: () => ['Flour', 'Milk', 'Eggs'],
    },
    dataOptions: {
      default: () => ['Percentage', 'Weight (g)'],
    },
  },
  methods: actionsData,
})
