import IngredientForm from './'

describe('IngredientForm', () => {
  it('exports a valid component', () => {
    expect(IngredientForm).toBeAComponent()
  })
})
