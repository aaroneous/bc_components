import IngredientRow from './'

describe('IngredientRow', () => {
  it('exports a valid component', () => {
    expect(IngredientRow).toBeAComponent()
  })
})
