import { action } from '@storybook/addon-actions'
import IngredientRow from './'

export default {
  title: 'IngredientRow',
  excludeStories: /.*Data$/,
}

export const actionsData = {
  onRemoveIngredient: action('onRemoveIngredient'),
}

export const ingredientData = {
  id: 0,
  name: 'Flour',
  weight: 100,
  ratio: 100,
}

const ingredientRowTemplate = `<IngredientRow :ingredient="ingredient" @removeIngredient="onRemoveIngredient" />`

export const Default = () => ({
  components: { IngredientRow },
  template: ingredientRowTemplate,
  props: {
    ingredient: {
      default: () => ingredientData,
    },
  },
  methods: actionsData,
})
