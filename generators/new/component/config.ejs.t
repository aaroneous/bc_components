---
to: "src/components/<%= name %>/package.json"
---
{
  "name": "@aaroneous/<%= h.changeCase.kebab(name) %>",
  "version": "0.0.0",
  "repository": {
    "type": "git",
    "url": "ssh://git@gitlab.com:@aaroneous/bc_components.git"
  },
  "files": [
    "dist"
  ],
  "scripts": {
    "build": "vue-cli-service build --target lib --inline-vue index.vue"
  },
}