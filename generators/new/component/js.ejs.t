---
to: "<%= typeof files != 'undefined' && files.indexOf('script') !== -1 ? `src/components/${name}/${name}.js` : null %>"
---
export default {
  name: '<%= name %>',
  data () {
    return {}
  }
}
