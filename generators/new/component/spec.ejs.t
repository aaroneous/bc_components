---
to: "<%= extras.indexOf('test') !== -1 ? `src/components/${name}/${name}.spec.js` : null %>"
---
import <%= name %> from './'

describe('<%= name %>', () => {
  it('exports a valid component', () => {
    expect(<%= name %>).toBeAComponent()
  })
})
