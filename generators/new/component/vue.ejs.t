---
to: 'src/components/<%= name %>/index.vue'
---
<%
if (files.indexOf('template') !== -1) {
%><template src="./<%= name %>.html" />
<%
}
else {
%><template>
  <div>
  </div>
</template>

<%
}

if (files.indexOf('script') !== -1) {
%><script src="./<%= name %>.js" />
<%
}
else {
%><script>
export default {
  name: '<%= name %>',
  data () {
    return {}
  }
}
</script>

<%
}

if (files.indexOf('style') !== -1) {
%><style src="./<%= name %>.scss" scoped lang="scss" />
<%
}
else {
%><style scoped lang="scss">
  
</style>
<%
}
%>
