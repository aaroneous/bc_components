const _ = require('lodash')

module.exports = {
  prompt: ({ inquirer, args }) => {
    const questions = []
    if (typeof args.name === 'undefined') {
      questions.push({
        type: 'input',
        name: 'name',
        message: 'Name:',
        validate(value) {
          if (!value.length) {
            return 'Components must have a name.'
          }
          const fileName = _.kebabCase(value)
          if (fileName.indexOf('-') === -1) {
            return 'Component names should contain at least two words to avoid conflicts with HTML elements.'
          }
          return true
        },
      })
    }

    return inquirer.prompt(questions).then((answers) => {
      const name = answers.name || args.name
      const questions = []

      questions.push(
        {
          type: 'multiselect',
          name: 'files',
          message: 'Select component sections to keep as separate files:',
          choices: [
            {
              name: 'style',
              message: `${name}.scss`,
            },
            {
              name: 'template',
              message: `${name}.html`,
            },
            {
              name: 'script',
              message: `${name}.js`,
            },
          ],
        },
        {
          type: 'multiselect',
          name: 'extras',
          message: 'Component Extras',
          choices: [
            {
              name: 'test',
              message: `${name}.spec.js`,
            },
            {
              name: 'story',
              message: `${name}.stories.js`,
            },
          ],
        }
      )

      return inquirer
        .prompt(questions)
        .then((nextAnswers) => Object.assign({}, answers, nextAnswers))
    })
  },
}
